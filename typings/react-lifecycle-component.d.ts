declare module 'react-lifecycle-component' {
  import { ComponentLifecycle } from 'react';
  import {
    DispatchProp,
    InferableComponentEnhancer,
    InferableComponentEnhancerWithProps,
    MapDispatchToPropsParam,
    MapStateToPropsParam,
    MergeProps,
    Options
  } from 'react-redux';

  export interface LifecycleStateProps<P = {}> {
    component: React.ComponentType<P>;
  }

  export interface LifecycleDispatchProps<P = {}, S = {}> extends ComponentLifecycle<P, S> {}

  export interface Props<P = {}, S = {}> extends LifecycleStateProps<P>, LifecycleDispatchProps<P, S> {}

  export class LifecycleComponent<P = {}, S = {}> extends React.Component<Props<P, S>> {}

  export function applyLifecycle<P = {}, S = {}>(
    component: React.ComponentType<P>
  ): React.ComponentType<P & LifecycleDispatchProps<P, S>>;

  export function connectWithLifecycle(): InferableComponentEnhancer<DispatchProp<any>>;

  export function connectWithLifecycle<TStateProps, no_dispatch, TOwnProps>(
    mapStateToProps: MapStateToPropsParam<TStateProps, TOwnProps, {}>
  ): InferableComponentEnhancerWithProps<TStateProps & DispatchProp<any>, TOwnProps>;

  export function connectWithLifecycle<TStateProps, TDispatchProps extends LifecycleDispatchProps<any>, TOwnProps>(
    mapStateToProps: MapStateToPropsParam<TStateProps, TOwnProps, {}>,
    mapDispatchToProps: MapDispatchToPropsParam<TDispatchProps, TOwnProps>
  ): InferableComponentEnhancerWithProps<TStateProps & TDispatchProps, TOwnProps>;

  export function connectWithLifecycle<
    TStateProps,
    no_dispatch,
    TOwnProps,
    TMergedProps extends LifecycleDispatchProps<any>
  >(
    mapStateToProps: MapStateToPropsParam<TStateProps, TOwnProps, {}>,
    mapDispatchToProps: null | undefined,
    mergeProps: MergeProps<TStateProps, undefined, TOwnProps, TMergedProps>
  ): InferableComponentEnhancerWithProps<TMergedProps, TOwnProps>;

  export function connectWithLifecycle<
    TStateProps,
    TDispatchProps extends LifecycleDispatchProps<any>,
    TOwnProps,
    TMergedProps extends LifecycleDispatchProps<any>
  >(
    mapStateToProps: MapStateToPropsParam<TStateProps, TOwnProps, {}>,
    mapDispatchToProps: MapDispatchToPropsParam<TDispatchProps, TOwnProps>,
    mergeProps: MergeProps<TStateProps, TDispatchProps, TOwnProps, TMergedProps>
  ): InferableComponentEnhancerWithProps<TMergedProps, TOwnProps>;

  export function connectWithLifecycle<TStateProps, no_dispatch, TOwnProps>(
    mapStateToProps: MapStateToPropsParam<TStateProps, TOwnProps, {}>,
    mapDispatchToProps: null | undefined,
    mergeProps: null | undefined,
    options: Options<TStateProps, TOwnProps>
  ): InferableComponentEnhancerWithProps<DispatchProp<any> & TStateProps, TOwnProps>;

  export function connectWithLifecycle<TStateProps, TDispatchProps extends LifecycleDispatchProps<any>, TOwnProps>(
    mapStateToProps: MapStateToPropsParam<TStateProps, TOwnProps, {}>,
    mapDispatchToProps: MapDispatchToPropsParam<TDispatchProps, TOwnProps>,
    mergeProps: null | undefined,
    options: Options<TStateProps, TOwnProps>
  ): InferableComponentEnhancerWithProps<TStateProps & TDispatchProps, TOwnProps>;

  export function connectWithLifecycle<
    TStateProps,
    TDispatchProps extends LifecycleDispatchProps<any>,
    TOwnProps,
    TMergedProps extends LifecycleDispatchProps<any>
  >(
    mapStateToProps: MapStateToPropsParam<TStateProps, TOwnProps, {}>,
    mapDispatchToProps: MapDispatchToPropsParam<TDispatchProps, TOwnProps>,
    mergeProps: MergeProps<TStateProps, TDispatchProps, TOwnProps, TMergedProps>,
    options: Options<TStateProps, TOwnProps, TMergedProps>
  ): InferableComponentEnhancerWithProps<TMergedProps, TOwnProps>;
}
