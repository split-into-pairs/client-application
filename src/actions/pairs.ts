import { IFailedPairsEvent, ILoadedPairsEvent, ITryToLoadPairsEvent, PairsActions } from "../constants/pairs";
import { IPair } from "../reducers/pairs";

export const tryToLoadPairs = (): ITryToLoadPairsEvent => ({
  type: PairsActions.TryToLoadPairs
});

export const loadedPairs = (pairs: IPair[]): ILoadedPairsEvent => ({
  pairs,
  type: PairsActions.PairsAreLoaded
});

export const cannotLoadPairs = (message: string): IFailedPairsEvent => ({
  message,
  type: PairsActions.CannotLoadPairs
});