import { ChangeSet } from '@devexpress/dx-react-grid';
import { EmployeesActions } from './../../constants/employees';
import { onGridCommitChanges } from './../employees';

it('should dispatch creating new employee from changeset', () => {
  const dispatch = jest
    .fn()
    .mockImplementationOnce(action => expect(action.type).toEqual(EmployeesActions.TryToRemoveEmployee))
    .mockImplementationOnce(action => expect(action.type).toEqual(EmployeesActions.TryToRemoveEmployee))
    .mockImplementationOnce(action => expect(action.type).toEqual(EmployeesActions.UpdateEmployee))
    .mockImplementationOnce(action => expect(action.type).toEqual(EmployeesActions.CreateEmployee))
    .mockImplementationOnce(action => expect(action.type).toEqual(EmployeesActions.CreateEmployee))


  const changes: ChangeSet = {
    added: [
      { name: 'Lorem ipsum', team: 'A', district: 'B', age: 25, badEyesight: false },
      { name: 'Dolor sit amet', team: 'B', district: 'B', age: 24, badEyesight: true }
    ],
    changed: {
      12: { name: 'ABCD' }
    },
    deleted: [1, 2]
  }

  onGridCommitChanges(dispatch)(changes)
})