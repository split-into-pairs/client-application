import { ChangeSet } from '@devexpress/dx-react-grid';
import { Dispatch } from 'redux';
import {
  EmployeesActions,
  ICreateEmployeeEvent,
  IFailedEmployeesEvent,
  ILoadedEmployeesEvent,
  IRemoveEmployeeEvent,
  ITryToLoadEmployeesEvent,
  ITryToRemoveEmployeeEvent,
  IUpdateEmployeeEvent
} from './../constants/employees';
import { IEmployee } from './../reducers/employees';

export const tryToLoadEmployees = (): ITryToLoadEmployeesEvent => ({
  type: EmployeesActions.TryToLoadEmployees
});

export const loadedEmployees = (employees: IEmployee[]): ILoadedEmployeesEvent => ({
  employees,
  type: EmployeesActions.EmployeesAreLoaded
});

export const cannotLoadEmployees = (message: string): IFailedEmployeesEvent => ({
  message,
  type: EmployeesActions.CannotLoadEmployees
});
export const createEmployee = (changes: object): ICreateEmployeeEvent => ({
  changes,
  type: EmployeesActions.CreateEmployee
});

export const tryToRemoveEmployee = (index: number): ITryToRemoveEmployeeEvent => ({
  index,
  type: EmployeesActions.TryToRemoveEmployee
});

export const removeEmployee = (index: number): IRemoveEmployeeEvent => ({
  index,
  type: EmployeesActions.RemoveEmployee
});

export const updateEmployee = (index: number, changes: object): IUpdateEmployeeEvent => ({
  changes,
  index,
  type: EmployeesActions.UpdateEmployee
});

export const onGridCommitChanges = (dispatch: Dispatch) => (changes: ChangeSet): void => {
  if (undefined !== changes.deleted && changes.deleted.length > 0) {
    changes.deleted.forEach((index: string) => {
      dispatch(tryToRemoveEmployee(Number(index)));
    });
  }

  if (undefined !== changes.changed) {
    Object.keys(changes.changed).forEach(
      (key: string): void => {
        dispatch(updateEmployee(Number(key), changes.changed && changes.changed[key]));
      }
    );
  }

  if (undefined !== changes.added && changes.added.length > 0) {
    changes.added.forEach(
      (values: object): void => {
        dispatch(createEmployee(values));
      }
    );
  }
};
