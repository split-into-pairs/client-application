import * as React from 'react';
import { Header, NavigationItem, NavigationList } from './theme';

interface INavbar {
  children: any;
}

const Navbar = ({ children }: INavbar) => (
  <Header>
    <NavigationList>{children}</NavigationList>
  </Header>
);

export default Navbar;
export { NavigationItem };
