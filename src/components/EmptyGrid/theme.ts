import styled, { css } from 'styled-components';

const common = css`
  height: 64px;
  line-height: 64px;
  text-align: center;
`;

export const TableRow = styled.tr`
  ${common};
`;

export const TableCell = styled.td`
  ${common} font-size: 12px;
`;
