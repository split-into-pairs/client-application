import urlBuilder from './urlBuilder'

export default function (changes: object): Promise<any> {
  return fetch(urlBuilder('/employees/'), {
    body: JSON.stringify(changes),
    method: 'POST'
  });
}
