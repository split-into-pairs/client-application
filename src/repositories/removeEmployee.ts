import urlBuilder from './urlBuilder'

export default function (id: number): Promise<any> {
  return fetch(urlBuilder(`/employees/${id}`), {
    method: 'DELETE'
  });
}
