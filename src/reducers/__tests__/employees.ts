import { fromJS } from 'immutable';
import { cannotLoadEmployees, loadedEmployees, updateEmployee } from '../../actions/employees';
import { EmployeesActions } from '../../constants/employees';
import reducer, { IEmployee, initialState } from './../employees';

it('should assign employees when system dispatch event', () => {
  const example: IEmployee[] = [
    { id: 12, name: 'Lorem ipsum', age: 25, badEyesight: false, district: 'AAA', team: 'BBB' }
  ];
  const result = reducer(initialState, loadedEmployees(example));
  expect(result.getIn(['employees']).size).toEqual(1);
});

it('should change status to loading when system dispatch event', () => {
  const result = reducer(initialState, { type: EmployeesActions.TryToLoadEmployees });
  expect(result.getIn(['status', 'isLoading'])).toBeTruthy();
});

it('should change status to not loading when employees are loaded', () => {
  let tmpState = initialState.setIn(['status', 'isLoading'], true);
  tmpState = initialState.setIn(['status', 'errMessage'], 'lorem ipsum');

  const result = reducer(tmpState, loadedEmployees([]));

  expect(result.getIn(['status', 'isLoading'])).toBeFalsy();
  expect(result.getIn(['status', 'errMessage'])).toEqual('');
});

it('should change status to not loading when cannot load employees', () => {
  const tmpState = initialState.setIn(['status', 'isLoading'], true);
  const result = reducer(tmpState, cannotLoadEmployees('lorem ipsum'));

  expect(result.getIn(['status', 'isLoading'])).toBeFalsy();
  expect(result.getIn(['status', 'errMessage'])).toEqual('lorem ipsum');
});

it('should update employee with proper index when event is dispatched', () => {
  const example: IEmployee[] = [
    { id: 12, name: 'Lorem ipsum', age: 25, badEyesight: false, district: 'AAA', team: 'BBB' },
    { id: 13, name: 'Lorem ipsum', age: 25, badEyesight: false, district: 'AAA', team: 'BBB' },
    { id: 14, name: 'Lorem ipsum', age: 25, badEyesight: false, district: 'AAA', team: 'BBB' }
  ];
  const tmpState = initialState.setIn(['employees'], fromJS(example));
  const result = reducer(
    tmpState,
    updateEmployee(1, {
      age: 24,
      name: 'Dolor sit amet'
    })
  );

  expect(result.getIn(['employees', 1, 'name'])).toEqual('Dolor sit amet');
  expect(result.getIn(['employees', 1, 'age'])).toEqual(24);
  expect(result.getIn(['employees', 1, 'district'])).toEqual('AAA');
});

it('should remove employee with proper index when event is dispatched', () => {
  const example: IEmployee[] = [
    { id: 12, name: 'Lorem ipsum', age: 25, badEyesight: false, district: 'AAA', team: 'BBB' },
    { id: 13, name: 'Lorem ipsum', age: 25, badEyesight: false, district: 'AAA', team: 'BBB' },
    { id: 14, name: 'Lorem ipsum', age: 25, badEyesight: false, district: 'AAA', team: 'BBB' }
  ];
  const tmpState = initialState.setIn(['employees'], fromJS(example));
  const result = reducer(tmpState, { type: EmployeesActions.RemoveEmployee, index: 1 });

  expect(result.getIn(['employees']).size).toEqual(2);
  result.getIn(['employees']).forEach(
    (e: IEmployee): void => {
      expect(e.id).not.toEqual(13);
    }
  );
});
