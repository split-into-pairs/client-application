import { fromJS, List } from 'immutable';
import { PairsAction, PairsActions } from './../constants/pairs';
import { IColumnGrid, IStatus } from './common';
import { IEmployee } from './employees';

export const initialState: List<IPairsState> = fromJS({
  columns: [
    { name: 'first', title: 'First Employee' },
    { name: 'second', title: 'Second Employee' }
  ],
  pairs: [],
  status: {
    errMessage: '',
    isLoading: false
  }
});

export default function (state: List<IPairsState> = initialState, action: PairsAction): List<IPairsState> {
  switch (action.type) {
    case PairsActions.TryToLoadPairs:
      return state.setIn(['status', 'isLoading'], true)
    case PairsActions.CannotLoadPairs:
      state = state.setIn(['status', 'isLoading'], false)
      state = state.setIn(['status', 'errMessage'], action.message)

      return state
    case PairsActions.PairsAreLoaded:
      state = state.setIn(['status', 'isLoading'], false)
      state = state.setIn(['pairs'], fromJS(action.pairs))

      return state
    default:
      return state;
  }
}

export interface IPairsState {
  columns: IColumnGrid[];
  pairs: IPair[];
  status: IStatus;
}

export interface IPair {
  first: IEmployee;
  second?: IEmployee;
}