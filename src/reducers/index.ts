import { Provider } from 'react-redux';
import { applyMiddleware, compose, createStore } from 'redux';
import { combineReducers } from 'redux-immutable';
import createSagaMiddleware from 'redux-saga';
import applySagas from './../sagas';
import employeesReducers, { IEmployeesState } from './employees';
import pairsReducers, { IPairsState } from './pairs';

const sagaMiddleware = createSagaMiddleware();

export default createStore(
  combineReducers({
    employees: employeesReducers,
    pairs: pairsReducers
  }),
  compose(
    applyMiddleware(sagaMiddleware)
  )
);

applySagas(sagaMiddleware);

export { Provider };
export interface IStore {
  employees: IEmployeesState;
  pairs: IPairsState
}