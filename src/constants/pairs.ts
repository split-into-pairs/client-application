import { Action } from 'redux';
import { IPair } from '../reducers/pairs';

export enum PairsActions {
  TryToLoadPairs = 'TRY_TO_LOAD_PAIRS',
  PairsAreLoaded = 'PAIRS_ARE_LOADED',
  CannotLoadPairs = 'CANNOT_LOAD_PAIRS',
}

export interface ITryToLoadPairsEvent extends Action<PairsActions.TryToLoadPairs> { }
export interface ILoadedPairsEvent extends Action<PairsActions.PairsAreLoaded> {
  pairs: IPair[];
}
export interface IFailedPairsEvent extends Action<PairsActions.CannotLoadPairs> {
  message: string;
}

export type PairsAction = ITryToLoadPairsEvent
  | ILoadedPairsEvent
  | IFailedPairsEvent