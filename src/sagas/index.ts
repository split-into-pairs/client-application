import { SagaMiddleware } from 'redux-saga'
import sagaEmployees from './employees';
import sagaPairs from './pairs'

export default function (middleware: SagaMiddleware<{}>) {
  middleware.run(sagaEmployees)
  middleware.run(sagaPairs)
}

export { sagaEmployees, sagaPairs };
