import { put, takeEvery } from 'redux-saga/effects';
import { ITryToLoadPairsEvent, PairsActions } from "../constants/pairs";
import apiFetchAll from '../repositories/fetchPairs';
import { cannotLoadPairs, loadedPairs } from './../actions/pairs';

export function* fetch(action: ITryToLoadPairsEvent) {
  try {
    const pairs = yield apiFetchAll();
    yield put(loadedPairs(pairs));
  } catch (err) {
    yield put(cannotLoadPairs(err.message));
  }
}

export default function* watchFetchEmployees() {
  yield takeEvery(PairsActions.TryToLoadPairs, fetch);
}
