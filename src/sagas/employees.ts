import { List } from 'immutable';
import { put, select, takeEvery } from 'redux-saga/effects';
import { cannotLoadEmployees, loadedEmployees, removeEmployee, tryToLoadEmployees } from '../actions/employees';
import { EmployeesActions, ICreateEmployeeEvent, IRemoveEmployeeEvent, ITryToLoadEmployeesEvent, IUpdateEmployeeEvent } from '../constants/employees';
import { IStore } from '../reducers';
import apiCreate from '../repositories/createEmployee';
import apiFetchAll from '../repositories/fetchEmployees';
import apiRemove from '../repositories/removeEmployee';
import apiUpdate from '../repositories/updateEmployee';

export function* fetch(action: ITryToLoadEmployeesEvent) {
  try {
    const employees = yield apiFetchAll();
    yield put(loadedEmployees(employees));
  } catch (err) {
    yield put(cannotLoadEmployees(err.message));
  }
}

export function* create(action: ICreateEmployeeEvent) {
  try {
    yield apiCreate(action.changes);
    yield put(tryToLoadEmployees());
  } catch (err) {
    yield put(tryToLoadEmployees());
  }
}

export function* update(action: IUpdateEmployeeEvent) {
  try {
    const employeeId = yield select(
      (state: List<IStore>): number => {
        return state.getIn(['employees', 'employees', action.index, 'id'], 0);
      }
    );

    yield apiUpdate(employeeId, action.changes);
  } catch (err) {
    yield put(tryToLoadEmployees());
  }
}

export function* remove(action: IRemoveEmployeeEvent) {
  try {
    const employeeId = yield select(
      (state: List<IStore>): number => {
        return state.getIn(['employees', 'employees', action.index, 'id'], 0);
      }
    );

    yield apiRemove(employeeId);
    yield put(removeEmployee(action.index));
  } catch (err) {
    yield put(tryToLoadEmployees());
  }
}

export default function* watchFetchEmployees() {
  yield takeEvery(EmployeesActions.TryToLoadEmployees, fetch);
  yield takeEvery(EmployeesActions.CreateEmployee, create);
  yield takeEvery(EmployeesActions.UpdateEmployee, update);
  yield takeEvery(EmployeesActions.TryToRemoveEmployee, remove);
}
