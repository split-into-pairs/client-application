import { List } from 'immutable';
import { Dispatch } from 'redux';
import { IStore } from '../../reducers';
import { IPair } from '../../reducers/pairs';
import { tryToLoadPairs } from './../../actions/pairs';
import { IColumnGrid, IStatus } from './../../reducers/common';

export interface IProps {
  employees: List<IPair[]>;
  columns: List<IColumnGrid[]>;
  status: IStatus;
}
export interface IActions extends React.ComponentLifecycle<IProps, {}> {
}

export function mapStateToProps(state: List<IStore>): IProps {
  return {
    columns: state.getIn(['pairs', 'columns']),
    employees: state.getIn(['pairs', 'pairs']).map((pair: List<IPair>) => ({
      first: pair.getIn(['first', 'name']),
      second: pair.getIn(['second', 'name'], '--')
    })),
    status: state.getIn(['pairs', 'status'])
  };
}

export function bindDispatchToProps(dispatch: Dispatch): IActions {
  return {
    componentWillMount: () => dispatch(tryToLoadPairs())
  };
}