import { Input } from '@material-ui/core';
import * as React from 'react';

interface IEditor {
  value: boolean;
  onValueChange: any;
}

const onChange = (onValueChange: any) => (event: any) => onValueChange(Number(event.target.value));

const NumberEditor = ({ value, onValueChange }: IEditor) => (
  <Input type={'number'} value={value} onChange={onChange(onValueChange)} style={{ width: '100%' }} />
);

export default NumberEditor;
