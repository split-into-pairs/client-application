import { Chip } from '@material-ui/core';
import * as React from 'react';

interface IFormatter {
  value: boolean;
}

const NumberFormatter = ({ value }: IFormatter) => <Chip label={value} />;

export default NumberFormatter;
