import { DataTypeProvider } from '@devexpress/dx-react-grid';
import * as React from 'react';
import BooleanEditor from './BooleanEditor';
import BooleanFormatter from './BooleanFormatter';

const BooleanTypeProvider = (props: any) => (
  <DataTypeProvider formatterComponent={BooleanFormatter} editorComponent={BooleanEditor} {...props} />
);

export default BooleanTypeProvider;
