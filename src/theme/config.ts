export default {
  colors: {
    black: '#000',
    primary: '#1fa597',
    white: '#fff'
  },
  content: {
    width: 960
  },
  navbar: {
    height: 64
  }
};
