import * as styledComponents from 'styled-components';
import { ThemedStyledComponentsModule } from 'styled-components';
import ThemeConfig from './config';
import ThemeInterface from './theme';

const {
  default: styled,
  css,
  injectGlobal,
  keyframes,
  ThemeProvider
} = styledComponents as ThemedStyledComponentsModule<ThemeInterface>;

export { css, injectGlobal, keyframes, ThemeProvider, ThemeConfig };
export default styled;

/* tslint:disable */
injectGlobal`
  * {
    margin: 0;
    padding: 0;

    box-sizing: border-box;
    font-family: 'Roboto', sans-serif;
  }

  p, a, strong, span {
    color: black;
  }

  a {
    text-decoration: none;
  }
`;
