import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter, NavLink } from 'react-router-dom';
import Content from './components/Content';
import Navbar, { NavigationItem } from './components/Navbar';
import Store, { Provider } from './reducers';
import registerServiceWorker from './registerServiceWorker';
import Routing from './router';
import { ThemeConfig, ThemeProvider } from './theme';

ReactDOM.render(
  <Provider store={Store}>
    <BrowserRouter>
      <ThemeProvider theme={ThemeConfig}>
        <React.Fragment>
          <Navbar>
            <NavigationItem>
              <NavLink to={'/'} strict={true} exact={true}>
                Employees
              </NavLink>
            </NavigationItem>
            <NavigationItem>
              <NavLink to={'/pairs'} exact={true}>
                Make pairs
              </NavLink>
            </NavigationItem>
          </Navbar>
          <Content>
            <Routing />
          </Content>
        </React.Fragment>
      </ThemeProvider>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
